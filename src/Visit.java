import java.util.Date;

public class Visit {
    Customer customer;
    Date date;
    double serviceExpense;
    double productExpense;

    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }
    public String getName() {
        return customer.getName();
    }
    public double getServiceExpense() {
        return serviceExpense;
    }
    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }
    public double getProductExpense() {
        return productExpense;
    }
    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }
    public double getTotalExpense() {
        return getProductExpense() + getServiceExpense();
    }
    @Override
    public String toString() {
        return "Visit[CustomerName=" + getName()
                + ",date=" + this.date
                + ",serviceExpense=" + getServiceExpense()
                + ",productExpense=" + getProductExpense()
                + ",totalExpense=" + getTotalExpense() + "]";
    }
}
