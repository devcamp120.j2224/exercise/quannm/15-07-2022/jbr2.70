import java.util.Date;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Customer customer1 = new Customer("Quan");
        Customer customer2 = new Customer("Boi");
        System.out.println(customer1 + "," + customer2);

        Visit visit1 = new Visit(customer1, new Date());
        Visit visit2 = new Visit(customer2, new Date());
        System.out.println(visit1 + "," + visit2);

        visit1.setProductExpense(20);
        visit1.setServiceExpense(15);
        visit2.setProductExpense(80);
        visit2.setServiceExpense(22.5);
        System.out.println(visit1 + "," + visit2);
        double total = visit1.getTotalExpense() + visit2.getTotalExpense(); 
        System.out.println("Tổng chi phí 2 chuyến đi=" + total);

    }
}